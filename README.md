# MAILONE.class.php

### Descripción general
Envía un correo utilizando la funcion MAIL nativa de PHP y utiliza un template para dar formato.

### Ideologia
El objetivo general es poder dar formato simple a un enmail enviado.

### Parámetros
* Datos (`array`) Este array debe poseer las siguientes estructura de claves y datos:
  * _"to"_ (`array`) Array con los email de destino
  * _"subject"_ (`string`) Asunto del email
  * _"message"_ (`array`)
    * _"pretitle"_ (`string`/opcional) texto pre-titulo
    * _"title"_ (`string`/opcional) texto titulo
    * _"subtitle"_ (`string`/opcional) texto sub-titulo
    * _"text"_ (`string`/opcional) texto descriptivo.
    * _"table"_ (`array`/opcional) Array sin limite que será mostrado en forma de tabla en dos columenas con los datos "KEY" | "VALUE"
      * _"campo01"_ (`string`) Texto campo 01
      * _"campo02"_ (`string`) Texto campo 02
    * _"comment"_ (`string`) texto comentario
    * _"url"_ (`string`/opcional) texto url
    * _"footer"_ (`string`/opcional) texto footer
  * _"recaptcha"_ (`string`/opcional) Cadena con respuest del widget de Google Recaptcha si es que se utiliza
* Template (`string`/default: _"default"_) Seleccion del template a utilizar
* Recaptcha (`bool`/default: _false_) Codigo de reCaptcha de Google
* Limpiar (`bool`/default: _false_) Espedifica si se deben limpiar los campos antes de procesarlos

### Ejemplo
```php
  require_once("../class/mailone.class.php");
  $mail=new mailone();

  $data=array(
    "to" => array("email@email.com"),
    "subject" => "Asunto del Email",
    "message" => array(
      "pretitle" => "texto pre-titulo",
      "title" => "texto titulo",
      "subtitle" => "texto sub-titulo",
      "text" => "texto descriptivo.",
      "table" => array(
        "nombre" => "Albert",
        "apellido" => "Einstein"
      ),
      "comment" => "texto comentario",
      "url" => "texto url",
      "footer" => "texto footer"
    ),
    "recaptcha" => "recaptchaCode"
  );
  $mail->send($data);
```

### Valores devueltos
* Correcto: **TRUE**
* Error: **FALSE**

### Pendientes
* detalle de Error
* configuracion de colores del template
