<?php
  require_once("../class/mailone.class.php");

  $mail=new mailone();

  $data=array(
    "to" => array("email@email.com"),
    "subject" => "Asunto del Email",
    "message" => array(
      "pretitle" => "texto pre-titulo",
      "title" => "texto titulo",
      "subtitle" => "texto sub-titulo",
      "text" => "texto descriptivo.",
      "table" => array(
        "nombre" => "Albert",
        "apellido" => "Einstein"
      ),
      "comment" => "texto comentario",
      "url" => "texto url",
      "footer" => "texto footer"
    ),
    "recaptcha" => "recaptchaCode"
  );


  $mail->send($data);
?>