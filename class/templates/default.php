<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body yahoo bgcolor="#f6f8f1" style="min-width: 100% !important; margin: 0; padding: 0;">
    <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td>
          <table bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
            <tr>
              <td bgcolor="#81B8FF" style="padding: 40px 30px 20px;">
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                  <tr>
                    <td height="70">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <?php if(isset($data['pretitle']) AND $data['pretitle']!==false AND $data['pretitle']!==""){ ; ?>
                          <tr>
                            <td style="font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px; padding: 0 0 0 3px;">
                              <?php echo $data['pretitle']; ?>
                            </td>
                          </tr>
                        <?php } ?>

                        <?php if(isset($data['title']) AND $data['title']!==false AND $data['title']!==""){ ; ?>
                          <tr>
                            <td style="color: #153643; font-family: sans-serif; font-size: 33px; line-height: 38px; font-weight: bold; padding: 5px 0 0;">
                              <?php echo $data['title']; ?>
                            </td>
                          </tr>
                        <?php } ?>

                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

            <?php if(isset($data['subtitle']) AND $data['subtitle']!==false AND $data['subtitle']!==""){ ; ?>
              <tr>
                <td style="border-bottom-width: 1px; border-bottom-color: #f2eeed; border-bottom-style: solid; padding: 30px; background-color: #eee;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #153643; font-family: sans-serif; font-size: 24px; line-height: 28px; font-weight: bold; padding: 0 0 15px;">
                        <?php echo $data['subtitle']; ?>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            <?php } ?>

            <tr>
              <td style="border-bottom-width: 1px; border-bottom-color: #f2eeed; border-bottom-style: solid; padding: 30px;">
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                  <tr>
                    <td>

                      <?php if(isset($data['text']) AND $data['text']!==false AND $data['text']!==""){ ; ?>
                        <p style="color: #153643; font-family: sans-serif; font-size: 16px; line-height: 22px;">
                          <?php echo $data['text']; ?>
                        </p>
                      <?php } ?>

                      <?php if(isset($data['table']) AND $data['table']!==false AND count($data['table'])>0){ ; ?>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td style="color: #153643; font-family: sans-serif; font-size: 16px; line-height: 22px;">
                              <?php
                                foreach($data['table'] as $clave => $valor){
                                  echo "<b>".$clave.":</b> ".$valor."<br>";
                                }
                              ?>
                            </td>
                          </tr>
                        </table>
                      <?php } ?>

                    </td>
                  </tr>
                </table>
              </td>
            </tr>

            <?php if(isset($data['comment']) AND $data['comment']!==false AND $data['comment']!==""){ ; ?>
              <tr bgcolor="#eee">
                <td style="color: #153643; font-family: sans-serif; font-size: 16px; line-height: 22px; padding: 30px;">
                  <?php echo $data['comment']; ?>
                </td>
              </tr>
            <?php } ?>

              <tr>
                <td bgcolor="#81B8FF" style="padding: 20px 30px 15px;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <?php if(isset($data['url']) AND $data['url']!==false AND $data['url']!==""){ ; ?>
                      <tr>
                        <td align="center" style="font-family: sans-serif; font-size: 14px; color: #ffffff;">
                          <?php echo $data['url']; ?><br/>
                        </td>
                      </tr>
                    <?php } ?>

                    <?php if(isset($data['footer']) AND $data['footer']!==false AND $data['footer']!==""){ ; ?>
                      <tr>
                        <td align="center" style="font-family: sans-serif; font-size: 14px; color: #ffffff;">
                          <?php echo $data['footer']." - ".date("Y"); ?><br/>
                        </td>
                      </tr>
                    <?php } ?>

                  </table>
                </td>
              </tr>

          </table>
        </td>
      </tr>
    </table>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<style type="text/css">
			body { margin: 0 !important; padding: 0 !important; min-width: 100% !important; }
			img { height: auto !important; }
			body[yahoo] .hide { display: none !important; }
			body[yahoo] .buttonwrapper { background-color: transparent !important; }
			body[yahoo] .button { padding: 0px !important; }
			body[yahoo] .button a { background-color: #e05443 !important; padding: 15px 15px 13px !important; }
			body[yahoo] .unsubscribe { display: block !important; margin-top: 20px !important; padding: 10px 50px !important; background: #2f3942 !important; border-radius: 5px !important; text-decoration: none !important; font-weight: bold !important; }
    </style>
  </body>
</html>