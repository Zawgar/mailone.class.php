<?php
  /*
  developed by Zawgar
  https://github.com/Zawgar/MAILONE.class.php
  */
  if(!class_exists('MailOne')){
    class MailOne {

			private $header="";
			public $to="email@email.com";
			public $subject="Asunto";
			public $from="";
			public $fields=array();
			public $msg="";
			public $template="";
			private $recaptchaSecretKey="";

      // propiedad de uso interno para almacenar errores y detalles de los mismos
      private $result=array("error" => false, "summary" => "");

      public function __construct(){}

			public function send($data, $template="default", $clear=false){
				if(is_array($data["to"])){
					$this->to=substr(implode(",", $data["to"]), 0, -1);
				}else{
					$this->to=$data["to"];
				}
				$this->from=$data["from"];
				$this->subject=$data["subject"];
				$this->fields=$data["message"];
				$this->template=$template;

				if(isset($data["recaptcha"])){
					if(!$this->checkReCaptcha()){
						$this->result["error"]=true;
						$this->result["summary"]="";
					}
				}

				if($clear AND !$this->result["error"]){
					if(!$this->clearFields()){
						$this->result["error"]=true;
						$this->result["summary"]="";
					}
				}

				if($this->result["error"]){
					return false;
				}else{
					$this->createHeader();
					$this->loadTemplate($data);

					return $this->emailSend();
				}

			}

			// Verifico el reCaptcha
			public function checkReCaptcha(){
				$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$this->recaptchaSecretKey."&response=".$this->recaptcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
				$responseData=json_decode($response);
				if($responseData->success){
					return true;
				}else{
					return false;
				}
			}

			// Cabecera
			public function createHeader(){
				$headerArray=array();
				$headerArray[]="MIME-Version: 1.0";
				$headerArray[]="Content-type: text/html; charset=utf-8";
				$headerArray[]="From: ".$this->from;
				$headerArray[]="Subject: {".$this->subject."}";
				$headerArray[]="X-Mailer: PHP/".phpversion();
				$this->header=implode("\r\n", $headerArray);
			}

			// Carga del template
			public function loadTemplate(){
				$data=$this->fields;
				ob_start();
				if(file_exists("templates/".$this->template.".php")){
					require_once("templates/".$this->template.".php");
				}else{
					require_once("templates/default.php");
				}
				$this->msg=ob_get_contents();
				ob_end_clean();
			}

			// Envío del correo
			public function emailSend(){
				return @mail($this->to, $this->subject, $this->msg, $this->header);
			}

			// Limpia los campos
			public function clearFields(){
				foreach($this->fields as $key => $value){
					// escapo los caracteres especiales y tags html
					$this->fields[$key]=htmlspecialchars(strip_tags($value));
				}
				return true;
			}

		}
	}
?>
